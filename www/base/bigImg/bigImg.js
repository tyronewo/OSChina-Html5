define(["text!../../base/bigImg/bigImg.html", "zepto", "../../base/openapi", '../../base/util'],
	function(bigImgHtml, $, OpenAPI, Util) {
		var BigImg = Backbone.View.extend({

			id: 'userInfoContent',

			initialize: function() {
				this.render();
			},

			render: function() {
				var me = this;

				$(window).unbind("resize", me.checkImgContent);

				//定义屏幕伸缩重新计算宽度
				$(window).resize(function() {
					me.checkImgContent();
				});
				this.el = bigImgHtml;
				$("body").append(this.el);

				$("#imgMasker").click(function() {
					me.hide();
				});

				$(".imgContainer").click(function() {
					me.hide();
				});

				$('.button-container').click(function() {

					var str = $(".bigImg").attr("src");

					cordova.exec(function(obj) {
							console.log("Success>>" + obj);
						},
						function(e) {
							console.log("Error: " + e);
						}, "DownLoad", "downloadpic", ["downloadpic", str]);
				})
			},

			checkImgContent: function() {
				var me = this;
				if ($("#imgContent").css("display") === "block") {
					me.resetPosition();
					$("#imgContent").show();
				}
			},

			resetPosition: function(img) {
				var windowWidth = $(window).width();
				var windowHeight = $(window).height();

				var imgWidth = img.ImgWidth;
				var imgHeight = img.ImgHeight;

				var left;
				var top;
				//宽度判断
				
				if (imgWidth < windowWidth * 0.8 && imgWidth >= windowWidth * 0.2) {
					$("#imgContent").width(imgWidth);
					left = (windowWidth - imgWidth) / 2;
				} else if (imgWidth < windowWidth * 0.2) {
					var smallImgW = imgWidth * 1.5;
					$("#imgContent").width(smallImgW);
					left = (windowWidth - smallImgW) / 2;
				} else {
					var screenWidth = windowWidth * 0.8;
					$("#imgContent").width(screenWidth);
					left = (windowWidth - screenWidth) / 2;
				}
				// 高度判断
				if (imgHeight < windowHeight * 0.7 && imgHeight >= windowHeight * 0.2) {
					$("#imgContent").height(imgHeight);
					top = (windowHeight - imgHeight) / 3;
				} else if (imgHeight < windowHeight * 0.2) {
					$("#imgContent").height(imgHeight);
					top = (windowHeight - imgHeight) / 2;
				} else {
					var imgBiLi = imgWidth / imgHeight;
					var windowBiLi = windowWidth / windowHeight;
					if (imgBiLi < 1 && imgHeight >= windowHeight) {
						var heightBili = windowHeight / imgHeight;
						var screenHeight = imgHeight * windowBiLi;
						$("#imgContent").height(screenHeight);
					} else {
						var screenHeight = windowHeight * 0.8;
						$("#imgContent").height(screenHeight);
					}
					top = (windowHeight - screenHeight) / 3;
				}
				var bContaiLeft = ($(window).width() - 200) / 2;
				$("#imgContent").css({
					"position": "fixed",
					"top": top,
					"left": left,
					"display": 'none'
				});
				$(".button-container ").css({
					"margin-top": "20px",
					"position": "fixed",
					"right": bContaiLeft,
					"bottom": "50px"
				});

				$("#imgContent").hide();

			},
			show: function(img) {
				var me = this;
				me.resetPosition(img)
				$("#imgContent").show();
				$(".button-container").show();
				$('#imgMasker').show()

			},
			hide: function() {
				$("#imgContent").hide();
				$(".button-container").hide();
				$('#imgMasker').hide()

			}

		});
		return BigImg;
	});