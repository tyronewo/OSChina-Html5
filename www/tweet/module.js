define(function(require) {
     var v1 = require('tweet/comment-reply');
     var v2 = require('tweet/hot-list');
     var v3 = require('tweet/index');
     var v4 = require('tweet/myself-list');
     var v5 = require('tweet/tweet-detail');
     var v6 = require('tweet/tweet-issue');
     var v7 = require('tweet/tweet-list');
     return {
         'comment-reply': v1,
         'hot-list': v2,
         'index': v3,
         'myself-list': v4,
         'tweet-detail': v5,
         'tweet-issue': v6,
         'tweet-list': v7
      };
});