define(function(require) {
     var v1 = require('question/index');
     var v2 = require('question/profession-list');
     var v3 = require('question/question-detail');
     var v4 = require('question/question-edit');
     var v5 = require('question/question-list');
     var v6 = require('question/share-list');
     var v7 = require('question/station-list');
     var v8 = require('question/synthesize-list');
     var v9 = require('question/question-tags');

     return {
         'index': v1,
         'profession-list': v2,
         'question-detail': v3,
         'question-edit': v4,
         'question-list': v5,
         'share-list': v6,
         'station-list': v7,
         'synthesize-list': v8,
         'question-tags':v9
      };
});