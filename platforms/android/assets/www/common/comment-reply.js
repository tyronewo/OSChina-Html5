define(['text!../common/comment-reply.html', "../base/openapi", '../base/util', '../base/login/login'],
	function(viewTemplate, OpenAPI, Util, Login) {
		return Piece.View.extend({
			id: 'tweet_comment-reply',
			login: null,
			events: {
				"click .backBtn": "goBack",
				"click .reply": "commentReply"
			},
			goBack: function() {
				history.back();
			},
			commentReply: function() {
				Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login.show();
				} else {
					this.commentReply1();
				}
			},
			commentReply1: function() {
				var replyCont = $(".replyTextarea").val();
				replyCont = replyCont.replace(/(\s*$)/g, ""); //去掉右边空格
				var isNotice = $(".replyNotice").is(":checked");;
				if (replyCont === "" || replyCont === " ") {
					new Piece.Toast('请输入评论内容');
				} else {
					var me = this;
					var userToken = Piece.Store.loadObject("user_token");
					var accessToken = userToken.access_token;
					var tweetId = Util.request("tweetId");
					var commentAuthorId = Util.request("commentAuthorId");
					var catalog = Util.request("catalog");
					var commentId = Util.request("commentId");
					var urlResource = "comment_reply";
					var isChecked = $('#replyNotice').is(":checked");;
					if (isChecked) {
						isChecked = 1;
					} else {
						isChecked = 0;
					}
					var options = {
						access_token: accessToken,
						id: tweetId,
						catalog: catalog,
						content: replyCont,
						isPostToMyZone: isChecked,
						authorid: commentAuthorId,
						replyid: commentId,
						dataType: 'jsonp'
					};
					if (catalog === "blog") {
						urlResource = "blog_comment_reply";
						options = {
							access_token: accessToken,
							blog: tweetId,
							content: replyCont,
							reply_user: commentAuthorId,
							reply_id: commentId,
							dataType: 'jsonp'
						};
					}
					Util.Ajax(OpenAPI[urlResource], "GET", options, 'json', function(data, textStatus, jqXHR) {
						console.info(data);
						
						if (data.error === "200") {
							new Piece.Toast('评论回复成功');
							setTimeout(function() {
								history.back();
							}, 1500);
						} else {
							new Piece.Toast(data.error_description);
						}

					}, null, null);

				}
			},
			render: function() {
				login = new Login();
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				//write your business logic here :)
				var dataSource = Util.request("dataSource");
				var commentList = Piece.Store.loadObject(dataSource);
				console.info("===================================");
				var commentId = Util.request("commentId");
				commentId = parseInt(commentId); //item.id为int类型 ，所以先将comentId转换成int类型
				var data = null;
				$.each(commentList, function(index, item) {
					if (item.id === commentId) {
						data = item;
					}
				});
				var catalog = Util.request("catalog"); //根据catalog判断加载哪个模板
				var templateId = "commentReplyTemplate";
				if (catalog === "blog") {
					templateId = "blogCommentReplyTemplate";
				}
				var commentReplyTemplate = $(this.el).find("#" + templateId).html();
				var commentReplyHtml = _.template(commentReplyTemplate, data);
				$("#mainTweet").html("");
				$("#mainTweet").append(commentReplyHtml);

				$('.replyTextarea').focus();
			}
		}); //view define

	});